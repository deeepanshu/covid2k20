import { CovidData } from 'src/app/models/covid-data.model';
import { CovidDataAction, CovidActionTypes } from '../actions/covid.actions';

export interface CovidState {
    data: CovidData,
    loading: false,
    error: undefined
}

const initialState: CovidState = {
    data: undefined,
    loading: false,
    error: undefined
}

export function CovidReducer(state: CovidState = initialState, action: CovidDataAction) {

    switch (action.type) {

        case CovidActionTypes.LOAD_COVID_DATA:
            return {
                ...state,
                loading: true
            }

        case CovidActionTypes.LOAD_COVID_DATA_SUCCESS:
            return {
                ...state,
                data: action.payload,
                loading: false
            }
        case CovidActionTypes.LOAD_COVID_DATA_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        default:
            return {
                ...state,
            }
    }

}