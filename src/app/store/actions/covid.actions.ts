import { Action } from '@ngrx/store';
import { CovidData } from '../../models/covid-data.model';

export enum CovidActionTypes {
    LOAD_COVID_DATA = '[COVID] Load Covid Data',
    LOAD_COVID_DATA_SUCCESS = '[COVID] Load Covid Data Success',
    LOAD_COVID_DATA_FAILURE = '[COVID] Load Covid Data Failure',
}

export class LoadCovidDataAction implements Action {
    readonly type = CovidActionTypes.LOAD_COVID_DATA;
}

export class LoadCovidDataSuccessAction implements Action {
    readonly type = CovidActionTypes.LOAD_COVID_DATA_SUCCESS;
    constructor(public payload: CovidData) { }
}

export class LoadCovidDataFailureAction implements Action {
    readonly type = CovidActionTypes.LOAD_COVID_DATA_FAILURE;
    constructor(public payload: Error) { }

}

export type CovidDataAction = LoadCovidDataAction |
    LoadCovidDataSuccessAction |
    LoadCovidDataFailureAction;