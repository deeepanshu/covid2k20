import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from '@ngrx/effects';
import { CovidService } from 'src/app/core/covid.service';
import { CovidDataAction, CovidActionTypes, LoadCovidDataAction, LoadCovidDataSuccessAction, LoadCovidDataFailureAction } from '../actions/covid.actions';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()

export class CovidEffects {
    @Effect()
    loadData$ = this.actions$
        .pipe(
            ofType<LoadCovidDataAction>(CovidActionTypes.LOAD_COVID_DATA),
            mergeMap(
                () => this.covidService.getData()
                    .pipe(
                        map(data => {
                            console.log(data);
                            return new LoadCovidDataSuccessAction(data)
                        }),
                        catchError(error => of(new LoadCovidDataFailureAction(error))
                        )
                    )
            )
        )

    constructor(private actions$: Actions, private covidService: CovidService) { }
}