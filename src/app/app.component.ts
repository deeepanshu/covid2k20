import { Component, OnInit } from '@angular/core';
import { CovidService } from './core/covid.service';
import { Observable } from 'rxjs';
import { CovidData } from './models/covid-data.model';
import { Store } from '@ngrx/store';
import { AppState } from './models/app-state.model';
import { LoadCovidDataAction } from './store/actions/covid.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  covidData$: Observable<CovidData>
  loading$: Observable<Boolean>
  error$: Observable<Error>

  constructor(private store: Store<AppState>) { }
  title = "Covid"
  ngOnInit() {
    this.covidData$ = this.store.select(store => store.covidData.data);
    this.loading$ = this.store.select(store => store.covidData.loading);
    this.error$ = this.store.select(store => store.covidData.error);

    this.store.dispatch(new LoadCovidDataAction());
  }

}
