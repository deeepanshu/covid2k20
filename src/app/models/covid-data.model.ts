export interface CovidData {
    success: boolean;
    data: Data;
    lastRefreshed: string;
    lastOriginUpdate: string;
}

export interface Data {
    source: string;
    lastRefreshed: string;
    summary: Summary;
    rawPatientData: RawPatientDatum[];
}

export interface RawPatientDatum {
    patientId: number;
    reportedOn: string;
    onsetEstimate: string;
    ageEstimate: string;
    gender: Gender;
    city: string;
    district: string;
    state: State;
    status: Status;
    notes: string;
    contractedFrom?: string;
    sources?: string[];
    nationality: string[];
    place_attributes: PlaceAttribute[];
    relationship: Relationship[];
    travel: string[];
}

export enum Gender {
    Female = "female",
    Male = "male",
}

export interface PlaceAttribute {
    is_foreign: boolean;
    place: string;
}

export interface Relationship {
    link: string;
    with: string[];
}

export enum State {
    AndamanAndNicobarIslands = "Andaman and Nicobar Islands",
    AndhraPradesh = "Andhra Pradesh",
    Bihar = "Bihar",
    Chandigarh = "Chandigarh",
    Chhattisgarh = "Chhattisgarh",
    Delhi = "Delhi",
    Empty = "",
    Goa = "Goa",
    Gujarat = "Gujarat",
    Haryana = "Haryana",
    HimachalPradesh = "Himachal Pradesh",
    JammuAndKashmir = "Jammu and Kashmir",
    Karnataka = "Karnataka",
    Kerala = "Kerala",
    Ladakh = "Ladakh",
    MadhyaPradesh = "Madhya Pradesh",
    Maharashtra = "Maharashtra",
    Manipur = "Manipur",
    Mizoram = "Mizoram",
    Odisha = "Odisha",
    Puducherry = "Puducherry",
    Punjab = "Punjab",
    Rajasthan = "Rajasthan",
    TamilNadu = "Tamil Nadu",
    Telangana = "Telangana",
    UttarPradesh = "Uttar Pradesh",
    Uttarakhand = "Uttarakhand",
    WestBengal = "West Bengal",
}

export enum Status {
    Deceased = "Deceased",
    Empty = "",
    Hospitalized = "Hospitalized",
    Migrated = "Migrated",
    Recovered = "Recovered",
}

export interface Summary {
    total: number;
}
