import { CovidState } from '../store/reducer/covid.reducer';

export interface AppState {
    readonly covidData: CovidState
}