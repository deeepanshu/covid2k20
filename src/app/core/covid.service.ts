import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CovidData } from '../models/covid-data.model';

@Injectable({
  providedIn: 'root'
})
export class CovidService {

  private url: string = "https://api.rootnet.in/covid19-in/unofficial/covid19india.org";

  constructor(private http: HttpClient) { }

  getData() {
    return this.http.get<CovidData>(this.url);
  }

}
